package emenu.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.PrimaryKey;




@PersistenceCapable
public class Order implements Serializable{
	
	final public static String KIND=Order.class.getCanonicalName();
	
	public static enum Status{
		CREATED, SUBMITTED, FINISHED, PAID
	}
	
	public class SubOrder{
		public long idDish;
		public long number;
		public String Comment;
	}
	
	@PrimaryKey
	public long id;
	
	public Status status;

	public long table;

	public Date dateCreate;
	public Date dateSubmitted;
	public Date dateFinished;
	
	Set<SubOrder> subOrders;
	
	private long price=-1;
	
	
	public long getPrice(){
		if(this.price>0){
			return this.price;
		}
		long total=0;
		for(SubOrder subOrder: subOrders){
			//total+=subOrder.number*
		}
		return total;
	}
	
	
	public void setPrice(long price){
	
		this.price=price;
	}

	/** dishID - dish number*/
	public Map<Long, Long> dishes;
	
	
	public List<Long> cookingTasks;
	
	//public List<Long> tasks;
	
	//public long customer;
	
	public Order(long table) {
		this();
		this.table = table;
		
	}
	
	
	public Order(){
		this.cookingTasks=new ArrayList<Long>();
		this.dishes = new HashMap<Long, Long>();
		
	}

	
	@Override
	public boolean equals(Object o) {
		if(o==null || o.getClass()!=this.getClass()){
			return false;
		}
		if(o==this){
			return true;
		}
		Order other=(Order) o;
		return other.id==this.id;
	}
	
	
	
	
		
}


