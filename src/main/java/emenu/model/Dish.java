package emenu.model;

import java.io.Serializable;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Extension;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;



@PersistenceCapable
@Extension(vendorName="datanucleus", key="read-only", value="true")
public class Dish implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3489358120638118850L;
	public final static String KIND=Dish.class.getCanonicalName();
	
	

	@PrimaryKey
	public long id;
	public String name;
	

	@Column(length=1000)
	public String desc="";
	
	@Column(length=1000)
	public String iconUrl="";
	
	@Column(length=1000)
	public String picUrl="";

	public long price;
	
	
	public Dish(){
	}
	

	
	public Dish(long id, String name, String desc,  String iconUrl, String picUrl, long price){
		this();
		this.id=id;
		this.name=name;
		this.desc=desc;
		this.iconUrl=iconUrl;
		this.picUrl=picUrl;
		this.price=price;
	}
	
	private Dish(long id){
		
	}
	
	@Override
	public String toString(){
		String s=String.format("[id: %d, name: %s, icon: %s, img: %s, price: $%.2f]", id, name, iconUrl, picUrl, (float)price/100);
		return s;
	}
	
	public static long getPrice(long id){
		Dish d=new Dish(id);
		return d.price;
	}
		
}
