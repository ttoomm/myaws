package emenu.model;

import java.util.Date;

public abstract class Task {
	long id;
	long idEmployee;
	boolean done;
	boolean submitted;
	Date dateCreate;
	Date dateStart;
	Date dateSubmit;
	Date dateDone;
}
