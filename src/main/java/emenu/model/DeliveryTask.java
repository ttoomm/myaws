package emenu.model;

import java.util.Date;

public class DeliveryTask {
	public static final String KIND = DeliveryTask.class.getCanonicalName();
	public static long status_ready = -1, status_onDelivery = 0,
			status_finished = 1, status_HandleByPayment = 2;

	public long id = -1;
	public long byWaiter = -1;
	public long tableId = -1;

	public long dishId = -1;

	public Date orderDate = new Date(Long.MAX_VALUE);
	
	public long orderId=-1;

	public long status = -1;

	public long cookingTaskId = -1;

	
	public DeliveryTask(){
		
	}
	

}
