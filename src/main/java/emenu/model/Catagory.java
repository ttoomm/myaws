package emenu.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.PrimaryKey;



@PersistenceCapable
@Extension(vendorName="datanucleus", key="read-only", value="true")
public class Catagory implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2551631080735967175L;
	
	public final static String KIND=Catagory.class.getCanonicalName();
	
	@PrimaryKey
	public long id=0;
	public String name="";
	public String desc="";
	public String pic="";
	public List<Long> dishes;
	
	
	public Catagory(){
		this.dishes=new ArrayList<Long>();
	}
	public Catagory(long id, String name, String desc, String pic, List<Long> dishes){
		this();
		this.id=id;
		this.name=name;
		this.desc=desc;
		this.pic=pic;
		this.dishes=dishes;
	}
	

	
}
