package emenu.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import emenu.model.Order.Status;


public class CookingTask extends Task implements Comparable<CookingTask>{
	public static final String KIND=CookingTask.class.getCanonicalName();
	static public final long FINISHED=1, WAITING=-1, PROCESSING=0, SEND_TO_DELIVER=2;
	
	public long status=WAITING; 
	/**Task ID*/
	public long id=-1; 
	public long dishId=-1;
	
	/**orderID--dish number*/
	public Map<Long, Long> quantity;
	public Date dateEarliest=new Date(Long.MAX_VALUE);	
	public long byChief=-1;
		
	
	
	public CookingTask(){
		this.quantity=new HashMap<Long, Long>();
	}
	public CookingTask(long dishId){
		this();
		this.dishId=dishId;
	}
	
	
	

	public void addOrder(Order order) throws Exception{
		if(this.status!=WAITING){
			throw new Exception("this task is finished, can't be adding more orders");
		}
		if(order.status==Status.SUBMITTED ){
			throw new Exception("order has been handled");
		} 
		if(!order.dishes.containsKey(this.dishId)){
			throw new Exception("the order doesn't contain the dish of this task");
		}
		
		this.quantity.put(order.id, order.dishes.get(this.dishId));
		
		
		if(order.dateCreate.before(this.dateEarliest)){
			this.dateEarliest=order.dateCreate;
		}
	}

	

	public long getTotal(){
		long s=0;
		for(long i: this.quantity.values()){
			s+=i;
		}
		return s;
	}
	
	
	public void setTotal(long total){
		
	}
	
	//put the task from waiting to executing
	void starting(long chief){
		this.byChief=chief;
		this.status=CookingTask.PROCESSING;
	};
		
	void finish(){
		this.status=CookingTask.FINISHED;
	};
	
	

	public int compareTo(CookingTask to) {
		if(this.status!=to.status){
			return this.status<to.status?1: -1;
		}
		if(this.dateEarliest!=to.dateEarliest) {
			return this.dateEarliest.before(dateEarliest)? 1: -1;
		}
		return this.dishId<this.dishId? 1:-1;
	}
	
}