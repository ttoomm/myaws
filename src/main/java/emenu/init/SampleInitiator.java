package emenu.init;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Transaction;

import org.datanucleus.transaction.TransactionManager;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration.Transition;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.Region;
import com.amazonaws.services.s3.transfer.PersistableTransfer;
import com.amazonaws.services.s3.transfer.Transfer;
import com.amazonaws.services.s3.transfer.Transfer.TransferState;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.services.s3.transfer.internal.S3ProgressListener;
import com.amazonaws.services.s3.transfer.internal.TransferStateChangeListener;
import com.amazonaws.services.s3.transfer.model.UploadResult;

import emenu.model.Catagory;
import emenu.model.Dish;

public class SampleInitiator {
	
	String bk_name="com.ii-menu.media";
	String propertyFilename="/home/hh/aws/datanucleus.properties";
	
	String dirMedia="/home/hh/aws/prj/start/media";
	
	Regions regions=Regions.US_WEST_2;
	Region region=Region.US_West_2;
	
	String puName="Emenu";
	
	static Logger log=Logger.getLogger(SampleInitiator.class.getCanonicalName());
	
	AmazonS3Client sc;
	
	SampleInitiator() throws IOException{
		MyCredential key=new MyCredential(MyCredential.file_s3_credential);
		this.sc=new AmazonS3Client(key);
		sc.configureRegion(this.regions);
	}
	
	
	void initBucket() throws IOException{
		
		TransferManager tx=new TransferManager(sc);
		
		List<Bucket> buckets=sc.listBuckets();
		boolean bucket_exists=false;
		for(Bucket b: buckets){
			log.info("find bucket, name: "+b.getName());
			if(b.getName().equals(this.bk_name)){
				log.info("bucket "+this.bk_name+" exists.");
				bucket_exists=true;
				break;
			}
		}
		
		if(!bucket_exists){
			CreateBucketRequest reqCreate=new CreateBucketRequest(this.bk_name).withCannedAcl(CannedAccessControlList.PublicRead);
			Bucket bu=sc.createBucket(reqCreate);
			if(bu!=null){
				log.info("create bucket: "+bu.getName());
			}else{
				String msg="fail to create bucket, name: "+this.bk_name;
				log.log(Level.SEVERE, msg);
				throw new IOException(msg);
			}
		}
	}
	
	void populateCatagoryTable(PersistenceManager pm, List<Catagory> cs){
		Transaction tc=pm.currentTransaction();
		tc.begin();
		pm.makePersistentAll(cs);
		tc.commit();
	}
	
	void populateDishTable(AmazonS3Client sc, PersistenceManager pm, List<Dish> dishes, String bk_name) throws AmazonServiceException, AmazonClientException, InterruptedException {
		
		
		TransferManager tm=new TransferManager(sc);
		
		for(Dish dish: dishes){
			File icon=new File(dish.iconUrl);
			File img=new File(dish.picUrl);
			log.info(String.format("start upload dish [%s] icon: %s, image: %s ", dish.name, icon.getAbsolutePath(), img.getAbsolutePath()));
			
			PutObjectRequest reqIcon=new PutObjectRequest(this.bk_name, "icon/"+icon.getName(), icon).withCannedAcl(CannedAccessControlList.PublicRead);
			PutObjectRequest reqImg=new PutObjectRequest(this.bk_name, "pic/"+img.getName(), img).withCannedAcl(CannedAccessControlList.PublicRead);
			
			LsnrUpload lsnrIcon=new LsnrUpload(dish, LsnrUpload.Type.ICON, sc, bk_name, reqIcon.getKey());
			LsnrUpload lsnrImg=new LsnrUpload(dish, LsnrUpload.Type.IMG, sc, bk_name, reqImg.getKey());

			Upload uploadIcon=tm.upload(reqIcon, lsnrIcon);
			Upload uploadImg=tm.upload(reqImg, lsnrImg);
			
			UploadResult rltIcon=uploadIcon.waitForUploadResult();
			UploadResult rltImg=uploadImg.waitForUploadResult();

		}
		for(Dish dish: dishes){
			pm.makePersistent(dish);
		}
		
	}
	
	private static class LsnrUpload implements S3ProgressListener{
		Dish dish;
		Type tp;
		AmazonS3Client sc;
		String bk_name;
		String key;
		static enum Type{
			ICON, IMG
		}
		public LsnrUpload(Dish dish, Type type, AmazonS3Client sc, String bk_name, String key){
			this.dish=dish;
			this.tp=type;
			this.sc=sc;
			this.bk_name=bk_name;
			this.key=key;
		}

		public void progressChanged(ProgressEvent e) {
			if(ProgressEventType.TRANSFER_COMPLETED_EVENT==e.getEventType()){
				if(Type.ICON==this.tp){
					dish.iconUrl=sc.getUrl(this.bk_name, this.key).toString();
				}
				if(Type.IMG==this.tp){
					dish.picUrl=sc.getUrl(this.bk_name, this.key).toString();
				}
			}
			if(ProgressEventType.TRANSFER_STARTED_EVENT==e.getEventType()){
				String msg=String.format("Dish %s: %s, total byte: %d, transferred: %d", this.tp.toString(), this.dish.name, e.getBytes(), e.getBytesTransferred());
				log.info(msg);
			}
			
		}

		public void onPersistableTransfer(
				PersistableTransfer persistableTransfer) {
			// TODO Auto-generated method stub
			
		}


		
	}
	
	void initiateData(Properties props) throws AmazonServiceException, AmazonClientException, InterruptedException{
		SampleData sample=new SampleData(this.dirMedia);
		List<Dish> dishes=sample.getSampleDataDish();
		List<Catagory> catagories=sample.getSampleDataCatagory();
		
		Map<String, String> m=new HashMap<String, String>();
		for(Object k: props.keySet()){
			m.put((String)k, (String)props.get(k));
		}
		
		PersistenceManagerFactory pmf=JDOHelper.getPersistenceManagerFactory(m, this.puName);
		//JDOHelper.getper
		log.info("-------------------------------------------------------------connection information: ");
		log.info(String.format("[drivername: %s, connection url: %s, username: %s]", 
				pmf.getConnectionDriverName(), pmf.getConnectionURL(), pmf.getConnectionUserName()));
				
		PersistenceManager pm=pmf.getPersistenceManager();
		this.populateCatagoryTable(pm, catagories);
		this.populateDishTable(sc, pm, dishes, this.bk_name);
		
		
		
	}
	
	
	
	
	public static void main(String[] args) throws IOException, AmazonServiceException, AmazonClientException, InterruptedException{
		Properties props=new Properties();
		
		SampleInitiator initiator=new SampleInitiator();
		
		props.load(new FileInputStream(initiator.propertyFilename));
		initiator.initBucket();
		initiator.initiateData(props);
	}

}
