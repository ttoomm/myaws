package emenu.init;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.amazonaws.auth.AWSCredentials;

public class MyCredential implements AWSCredentials{
	
	public static String file_s3_credential="/home/hh/aws/credentials.csv";
		
	public MyCredential(String File) throws IOException{
		BufferedReader fr=new BufferedReader(new FileReader(File));
		String s1=fr.readLine();
		String s2=fr.readLine();
		String[] ss=s2.split(",");
		if(ss.length!=3){
			throw new IOException("File is not a credentials file");
		}
		
		this.awsAccessKeyId=ss[1];
		this.awsSecretAccessKey=ss[2];
	}
	private String awsAccessKeyId;
    private String awsSecretAccessKey;

	public String getAWSAccessKeyId() {
		// TODO Auto-generated method stub
		return awsAccessKeyId;
	}

	public String getAWSSecretKey() {
		// TODO Auto-generated method stub
		return awsSecretAccessKey;
	}
	
}