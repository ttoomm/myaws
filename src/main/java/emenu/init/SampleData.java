package emenu.init;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import emenu.model.Catagory;
import emenu.model.Dish;

public class SampleData implements Serializable {

	/**
	 *  
	 */
	private static final long serialVersionUID = -2197423233248194537L;

	private String dirMedias="";
	
	
	
	static Logger log=Logger.getLogger(SampleData.class.getCanonicalName());
	
	public SampleData(String dirMedia){
		this.dirMedias=dirMedia;
	}
	
	private String[][] getChickenDishes() {
		return new String[][] {

				new String[] { "Honey Sesame Chicken", "Orange Chicken",
						"SweetFire Chicken Breast",
						"String Bean Chicken Breast", "Kung Pao Chicken",
						"Mushroom Chicken", "Black Pepper Chicken",
						"Grilled Teriyaki Chicken", },

				new String[] {
						"Honey Sesame Chicken Breast is made with thin, crispy strips of all-white meat chicken tossed with fresh-cut string beans, crisp yellow bell peppers in a sizzling hot wok with our new delicious honey sauce and topped off with sesame seeds.",
						"Orange Chicken is a dish inspired by the Hunan Province in South Central China. It is prepared with crispy boneless chicken bites, tossed in the wok with our secret sweet and spicy orange sauce. Panda's very own Executive Chef Andy brought this entree to life and it quickly became Panda's most beloved dish.",
						"SweetFire Chicken Breast features crispy, white-meat chicken bites tossed in the wok with red bell peppers, diced onions and juicy pineapple with a zesty, sweet chili sauce inspired by the flavors of Thailand.",
						"String Bean Chicken Breast is a Cantonese dish inspired by the Guangdong Province on the Southern Coast of China. It is prepared with marinated chicken breast, fresh-cut string beans and sliced onions, tossed in the wok with a mild ginger soy sauce.",
						"Kung Pao Chicken is a spicy stir-fry dish inspired by the Sichuan Province in Central China. It is prepared with marinated diced chicken, crunchy peanuts, diced red bell peppers and sliced zucchini, all tossed in the wok with fresh green onions.",
						"Mushroom Chicken is a Cantonese dish inspired by the Guangdong Province on the Southern Coast of China. It is prepared with marinated diced chicken, sliced zucchini and button mushrooms, all tossed in the wok with a mild ginger soy sauce.",
						"Black Pepper Chicken is a traditional dish inspired by the Hunan Province in South Central China. It is prepared with marinated diced chicken, chopped celery, sliced onions and fresh ground black pepper, tossed in the wok with a mild ginger soy sauce.",
						"Grilled Teriyaki Chicken features marinated chicken filets that are grilled to perfection then sliced to order and served with our sweet and savory sauce. Availability of Grilled Teriyaki Chicken may vary by location.  View our Nutritional & Allergen PDF for details."

				} };
	}

	private String[][] getBeefDishes() {
		return new String[][] {
				// -------name
				new String[] { "Broccoli Beef", "Shanghai Angus Steak",
						"Beijing Beef", },
				new String[] {
						"Broccoli Beef is a Cantonese dish inspired by the Guangdong Province on the Southern Coast of China. It is prepared with marinated sliced beef and fresh broccoli florets, tossed in the wok with a mild ginger soy sauce.",
						"Shanghai Angus Steak features thick-cut slices of marinated Angus Top Sirloin with crisp asparagus, freshly sliced mushrooms, all wok-tossed in our new zesty Asian steak sauce.",
						"Beijing Beef is a Sichuan-style dish inspired by Central China. It is prepared with crispy strips of marinated beef, bell peppers and sliced onions, tossed in the wok with a tangy sweet and spicy sauce."

				}

		};
	}

	private String[][] getSeafoodDishes() {
		return new String[][] {
				// -------name
				new String[] { "Honey Walnut Shrimp", },

				new String[] { "Honey Walnut Shrimp is a dish inspired by the shores of Shanghai. It features fresh tempura shrimp wok-tossed in a gourmet honey sauce and topped with glazed walnuts. It's a mouthwatering combination of sweet and crispy." }

		};
	}

	private String[][] getRegionEntires() {
		return new String[][] {
				// -------name
				new String[] { "Eggplant Tofu", },
				new String[] { "Eggplant Tofu is a dish inspired by the Sichuan Province in Central China prepared with lightly browned tofu, fresh eggplant and diced red bell peppers, tossed in the wok with a sweet and spicy sauce." }

		};
	}

	private String[][] getSidesDishes() {
		return new String[][] {
				// -------name
				new String[] { "Chow Mein", "Fried Rice", "Mixed Veggies",
						"White Steamed Rice", "Brown Steamed Rice", },
				new String[] {
						"Chow Mein is prepared with our noodles, tossed in the wok with shredded onions, crisp celery and fresh cabbage.",
						"Fried Rice is prepared with steamed white rice that is tossed in the wok with soy sauce, scrambled eggs, green peas, carrots and chopped green onions.",
						"Mixed Veggies is a stir-fry combination of fresh broccoli, zucchini, carrots, string beans and cabbage.",
						"White Steamed Rice is prepared by steaming white rice to perfection.",
						"Brown Steamed Rice is prepared by steaming brown rice to perfection." }

		};
	}

	private String[][] getAppettizers() {
		return new String[][] {
				// -------name
				new String[] { "Chicken Egg Roll", "Veggie Spring Roll",
						"Hot and Sour Soup", "Chicken Potsticker",
						"Crispy Shrimp", "Cream Cheese Rangoon", },
				new String[] {
						"Chicken Egg Rolls are prepared with a mixture of cabbage, carrots, mushrooms, green onions and marinated chicken wrapped in a thin wonton wrapper and cooked to a golden brown.",
						"Veggie Spring Rolls are prepared with a mixture of cabbage, celery, carrots, green onions and Chinese noodles wrapped in a thin wonton wrapper and cooked to a golden brown.",
						"Hot & Sour Soup is a traditional Chinese soup prepared with vegetable stock, eggs, tofu and button mushrooms.",
						"Chicken Potstickers are prepared with a soft white dumpling filled with a combination of chicken, cabbage and onions that is pan seared on one side to a golden brown.",
						"Crispy Shrimp is prepared with marinated, butterflied shrimp that are cooked to a crispy golden brown.",
						"Cream Cheese Rangoons are prepared with a crisp wonton wrapper filled with a mixture of soft cream cheese and green onions, served with a side of sweet and sour sauce for dipping." }

		};
	}

	private String[][] getDesserts() {
		return new String[][] {
		// -------name
		new String[] { "Fortune Cookies", "Chocolate Chunk Cookie", },

		};
	}

	private String[][] getDrink() {
		return new String[][] {
		// -------name
		new String[] { "Pepsi", "Diet Pepsi", "Dr Pepper", "Sierra Mist",
				"Mountain Dew", "Mug Root Beer", "Sobe Lean",
				"Lipton Brisk Raspberry", "Lipton No Calorie Brisk Peach",
				"Tropicana Lemonade", "Tropicana Pink Lemonade",
				"Tropicana Fruit Punch", "Aquafina", "Gatorade Lemon-Lime",
				"Izze Sparkling Blackberry", "Ocean Spray Apple Juice",
				"SoBe Green Tea"} 
		};
	}

	private String[] getCatagories() {
		return new String[] { "Chicken Entrees", "Beef Entrees",
				"Seafood Entrees", "Regional Entrees", "Sides", "Appetizers",
				"Desserts", "Drinks" };

	}

	private List<Dish> dishes = new ArrayList<Dish>();
	private List<Catagory> cats = new ArrayList<Catagory>();

	void initData() {
		String[] strCats = this.getCatagories();
		String[][][] dishesData = new String[][][] { this.getChickenDishes(),
				this.getBeefDishes(), this.getSeafoodDishes(),
				this.getRegionEntires(), this.getSidesDishes(),
				this.getAppettizers(), this.getDesserts(), this.getDrink() };

		for (int i = 0; i < strCats.length; i++) {
			String nameCat = strCats[i];
			List<Long> dishIds = new ArrayList<Long>();

			for (int j = 0; j < dishesData[i][0].length; j++) {

				long idDish = (i + 1) * 10 + (j + 1);
				String name = dishesData[i][0][j];
				String icon = this.getDishMediaPath(idDish, MediaType.ICON);
				String pic = this.getDishMediaPath(idDish, MediaType.IMG);
				String desc = dishesData[i].length == 1 ? ""
						: dishesData[i][1][j];

				// assume the price is between $1 to $10
				int price = (int) (Math.random() * 1000);
				Dish dish = new Dish(idDish, name, desc, icon, pic, price);
				log.info("create dish:\n "+ dish.toString());

				dishIds.add(idDish);
				this.dishes.add(dish);
			}
			Catagory cat = new Catagory(i + 1, nameCat, "", "", dishIds);
			log.info("create catagory " + nameCat);
			this.cats.add(cat);

		}
	}
	
	static enum MediaType{
		ICON, IMG;
	}
	
	private String getDishMediaPath(long id, MediaType tp){
		String filePath="";
		
		if(tp==MediaType.ICON){
			filePath=String.format("%s/icons/icon_%d.png", this.dirMedias, id);
		}
		if(tp==MediaType.IMG){
			filePath=String.format("%s/images/pic_%d.jpg", this.dirMedias, id);
		}
		return filePath;
	}
	
	public List<Catagory> getSampleDataCatagory(){
		if(this.cats.size()==0){
			this.initData();
		}
		return this.cats;
	}
	
	public List<Dish> getSampleDataDish(){
		if(this.dishes.size()==0){
			this.initData();
		}
		return this.dishes;
	}
	
}
