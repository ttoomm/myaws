package tmp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.http.entity.ContentType;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerConfiguration;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.services.s3.transfer.model.UploadResult;

import emenu.init.MyCredential;

public class S3Testing implements ProgressListener{
	AWSCredentials key;
	
	S3Testing() throws IOException{
		 key=new MyCredential(MyCredential.file_s3_credential);
	
	}
	 
	String fileUpload="/home/hh/aws/prj/start/src/main/java/"+S3Testing.class.getName().replace(".", "/")+".java";
	String bk_name="com.ii-menu.test";
	String bk_nameStore="com.ii-menu.testa";
	
	public void log(String s){
		System.out.println(s);
	}
	
	void low() throws IOException{
		
		 
		AmazonS3Client sc=new AmazonS3Client(key);
		sc.configureRegion(Regions.US_WEST_2);
		//sc.setRegion(Region.getRegion(Regions.US_WEST_2));

		 
		 
		 //CreateBucketRequest reqCreate=new CreateBucketRequest(bk_name,	 com.amazonaws.services.s3.model.Region.US_West_2);
		CreateBucketRequest reqCreate=new CreateBucketRequest(bk_name);//,	 com.amazonaws.services.s3.model.Region.US_West_2);
		 Bucket bu=sc.createBucket(reqCreate);//, com.amazonaws.services.s3.model.Region.US_West_2);
		 if(bu!=null){
			 log("create bucket: "+bk_name);
		 }
		 else{
			 log("fail to create bucket: "+bk_name);
		 }
		 
		 //reqCreate=new CreateBucketRequest(bk_nameStore, Region.US_West_2);
		 //sc.createBucket(reqCreate);
		 
		 PutObjectRequest reqUpload=new PutObjectRequest(bk_nameStore, "jj.java", new File(fileUpload)).withCannedAcl(CannedAccessControlList.PublicRead);
		 ObjectMetadata meta=new ObjectMetadata();
		 meta.setContentType(ContentType.DEFAULT_TEXT.getMimeType());

		 
		 reqUpload.setMetadata(meta);
		 PutObjectResult rlt=sc.putObject(reqUpload);
		 meta=rlt.getMetadata();
		 
		 
		 String msg=String.format("upload result: etag: %s, disposition: %s, encoding: %s, size: %d, type: %s", 
				 rlt.getETag(), meta.getContentDisposition(), meta.getContentEncoding(), meta.getContentLength(), meta.getContentType());
		 log(msg);
		 String url=sc.getResourceUrl(bk_nameStore, "jj.java");
		 log(url);
		 
		 
		 
		 List<Bucket> bus=sc.listBuckets();
		 for(Bucket b: bus){
			 String info=String.format("get bucket info: name - %s, owner: %s, id: %s;", b.getName(), b.getOwner().getDisplayName(), b.getOwner().getId());
			 log(info);
		 }
		 
		 sc.deleteBucket(bk_name);
		 log("delete bucket");

	}
	
	void high() throws AmazonServiceException, AmazonClientException, InterruptedException{
		AmazonS3Client sc=new AmazonS3Client(key);
		//sc.setRegion(Region.getRegion(Regions.US_WEST_2));
		
		sc.configureRegion(Regions.US_WEST_2);
		TransferManager tx=new TransferManager(sc);
		
		
		//TransferManagerConfiguration tc=new TransferManagerConfiguration();
		
		File f=new File(this.fileUpload);
		Upload upload=tx.upload(this.bk_nameStore, f.getName(), f);
		upload.addProgressListener(this);

		
		UploadResult rlt=upload.waitForUploadResult();
		
		log("upload done");
		tx.shutdownNow();
	}
	
    // convert the bucket to lowercase for vanity domains
    // the bucket name must be lowercase since DNS is case-insensitive
    //static final String bucketName = awsAccessKeyId.toLowerCase() + "-test-bucket";
    //static final String keyName = "test-key";
    //static final String copiedKeyName = "copy-of-" + keyName;
	
	public static void main(String[] args) throws IOException, AmazonServiceException, AmazonClientException, InterruptedException{
		
		 S3Testing t=new S3Testing();
		 t.high();
		 //t.low();
		 
		
	}

	public void progressChanged(ProgressEvent e) {
		
		log(String.format("sending: %d, done: %d", e.getBytes(), e.getBytesTransferred()));
		
	}

}
